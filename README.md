# Tweet Button

Sharing products:

	echo Mage::helper('tweetbutton/product')->init($_product)

Sharing everything else:

	echo Mage::helper('tweetbutton')->($shareurl, $parms)

`$shareurl` can be null, or the URL to be shared/tweeted. If it's null, the current URL will be taken.
`$parms` can be null, or an associative array.

For possible parameters, see https://dev.twitter.com/docs/tweet-button#properties

## Admin options

- Via - Screen name of the user to attribute the Tweet to
- Hashtags - Comma separated hashtags appended to tweet text
- Text to prepend to the tweet (for sharing products)