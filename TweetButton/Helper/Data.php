<?php

class Doghouse_TweetButton_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getDefaultShareUrl()
    {
        return Mage::helper('core/url')->getCurrentUrl();
    }

    public function getHashTags()
    {
        return Mage::getStoreConfig('tweetbutton/general/hashtags');
    }

    /**
     * If shareurl, is empty, it uses the current URL
     *
     * For possible parameters, see https://dev.twitter.com/docs/tweet-button#properties
     *
     * @param  [String] Share url
     * @param  [Array] $data options array
     * @return [String]       url
     */
    public function generateUrl($shareurl = null, $data = array())
    {

        if(!$shareurl) {
            $shareurl = $this->getDefaultShareUrl();
        }

        $url = sprintf("https://twitter.com/share?url=%s", urlencode($shareurl));

        if($this->getHashTags()) {
            $url .= sprintf(
                "&amp;%s=%s",
                'hashtags',
                urlencode($this->getHashTags())
            );
        }

        if(is_array($data)) {
            foreach($data as $key => $value) {
                $url .= sprintf("&amp;%s=%s",
                    urlencode($key),
                    urlencode($value)
                );
            }
        }

        return $url;
    }

}
