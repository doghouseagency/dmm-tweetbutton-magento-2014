<?php

/**
 * @todo  add rich pinning: https://developers.pinterest.com/rich_pins/
 */
class Doghouse_TweetButton_Helper_Product extends Mage_Core_Helper_Abstract
{

    public function init(Mage_Catalog_Model_Product $product)
    {
        $this->_product = $product;
        return $this;
    }

    public function getProductTweetText()
    {
        return Mage::getStoreConfig('tweetbutton/product/text');
    }

    public function getViaHandle()
    {
        return Mage::getStoreConfig('tweetbutton/general/via');
    }

    public function __toString()
    {

        $p = $this->_product;

        $name = Mage::helper('catalog/output')->productAttribute($p, $p->getName(), 'name');

        $parms = array(
            'text' => sprintf('%s %s', $this->getProductTweetText(), $name),
            'counturl' => Mage::getBaseUrl()
        );

        if($this->getViaHandle()) {
            $parms['via'] = $this->getViaHandle();
        }

        //Instead, lets get the canonical URL of a product without the category path to make users
        //have more available characters in a tweet (since we aren't using a url shortener)
        //http://www.magentogoreview.com/get-canonical-url-of-a-product-in-phtml~225
        //Leave this off for now
        /*try {
            $initialUrl = $p->getProductUrl();
            $p->unsUrl();
            $shareurl = $p->getUrlModel()->getUrl($p, array('_ignore_category'=>true));
            $p->setUrl($initialUrl);
        } catch(Exception $e) {
            Mage::logException($e);
            $shareurl = $initialUrl;
        }*/

        return Mage::helper('tweetbutton')->generateUrl($p->getProductUrl(), $parms);

    }

}